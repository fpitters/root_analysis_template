/********************************************************************
 * File: common.h
 * ------------------------
 *
 * Description:
 * Common functions.
 *
 *
 * Version:
 * Author: Florian Pitters
 *
 *******************************************************************/

#ifndef __COMMON_H_INCLUDED__
#define __COMMON_H_INCLUDED__

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <list>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "TBranch.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TFile.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
#include "TFrame.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1.h"
#include "TH2.h"
#include "TH2Poly.h"
#include "TH3.h"
#include "THStack.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TMultiGraph.h"
#include "TPad.h"
#include "TPaveLabel.h"
#include "TProfile.h"
#include "TROOT.h"
#include "TRandom.h"
#include "TStyle.h"
#include "TTree.h"

using namespace std;



// --------------------------------------------------------
// Configuration
// --------------------------------------------------------

const string home_path = getenv("HOME");

const string cfg_path = "./config/";
const string res_path = "./data/";
const string dat_path = "./data/";

// flags
const int fSave = 1;




// --------------------------------------------------------
// Methods
// --------------------------------------------------------


struct fitResult {
    Float_t mean;
    Float_t mean_err;
    Float_t std;
    Float_t std_err;
    Float_t chi2;
    Float_t ndf;
};

void test_call_by_reference(TGraph& gr);
void test_smart_pointer(std::shared_ptr<TGraph> gr);

float gauss(float* x, float* par);
float mean(std::vector<float> vals);
float mean_last_x(std::vector<float> vals, int x);
float var(std::vector<float> vals);
float var_last_x(std::vector<float> vals, int x);
int find_first_above_x(std::vector<float> vals, float thr, bool reverse);
int find_extremum(std::vector<float> vals, int direction = 1);
void sort_array(std::vector<float> vals);
double find_closest_val(std::vector<double> vals, double x);
int find_closest_bin(std::vector<double> vals, double x);

std::vector<float> split_vector(std::vector<float> vals);

std::shared_ptr<fitResult> fitGauss(std::shared_ptr<TH1> hist1d, float fit_scale, float low, float high);
std::shared_ptr<fitResult> fitLandau(std::shared_ptr<TH1> hist1d, float fit_scale, float low, float high);

int read_file(std::string fn, char delim, std::vector<std::vector<float>> &dat, bool info);
int write_file(std::string fn, char delim, std::vector<std::vector<float>> &dat, std::string hd, bool info);
int read_array(std::string fn, char delim, std::vector<float> &dat, bool info);

std::shared_ptr<TGraphErrors> getProjectionFrom2DAlongY(std::shared_ptr<TH2F> scatter_plot, int nbins, float vmin, float vmax, int fit_nmin);
	
void printGraph(std::shared_ptr<TGraph> gr, string fn, string title, string titleX, string titleY, 
    float limX = -1.0, float limY = -1.0, float offset = 1.1, string option = "AP");
void printGraph(std::shared_ptr<TGraphErrors> gr, string fn, string title, string titleX, string titleY, 
    float limX = -1.0, float limY = -1.0, float offset = 1.1, string option = "APE");
void printMap(std::shared_ptr<TH2> hist2d, string fn, string title, string titleX, string titleY, string titleZ,
    float limX = -1.0, float limY = -1.0, float offsetY = 1.0, float offsetZ = 1.0, string option = "COLZ");
void printHist(std::shared_ptr<TH1> hist1d, string fn, string title, string titleX, string titleY, 
    float limX = -1.0, float limY = -1.0, float offset = 1.1, string option = "");


void addGraph(std::shared_ptr<TMultiGraph> mg, std::shared_ptr<TLegend> lg, int nentries, double* x, double* y,
        Color_t c, Style_t m, Size_t ms, Style_t ls, string style, string label);
void addErrGraph(std::shared_ptr<TMultiGraph> mg, std::shared_ptr<TLegend> lg, int nentries, double* x, double* y, 
    double* x_err, double* y_err, Color_t c, Style_t m, Size_t ms, Style_t ls, string style, string label);
void addFittedGraph(std::shared_ptr<TMultiGraph> mg, std::shared_ptr<TLegend> lg, int nentries, double* x, double* y,
    Color_t c, Style_t m, Size_t ms, Style_t ls, string style, string label, string func, double fit_low, double fit_up);
void addScatterGraph(std::shared_ptr<TH2> scatter, std::shared_ptr<TLegend> lg, 
    Color_t c, Style_t m, Size_t ms, string style, string label);
void addHist(std::shared_ptr<THStack> hs, std::shared_ptr<TLegend> lg, std::shared_ptr<TH1> hist1d,
    Color_t c, Style_t m, Style_t ls, string style, string label, double scale);	


void styleGraph();
void styleMap();
void styleCommon();

#endif

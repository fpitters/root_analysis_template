/********************************************************************
 * File: waveform.cc
 * ------------------------
 *
 * Description:
 * Tools to analyse raw waveforms.
 *
 *
 * Version:
 * Author: Florian Pitters
 *
 *******************************************************************/

#include "waveform.h"

using namespace std;



// --------------------------------------------------------
// Defintions
// --------------------------------------------------------

int findPeak(std::vector<float> pulse, std::string polarity){
    int index = -1;
    if (polarity == "neg"){
        index = std::distance(pulse.begin(), std::min_element(pulse.begin(), pulse.end()));
    }
    else if (polarity == "pos") {
        index = std::distance(pulse.begin(), std::max_element(pulse.begin(), pulse.end()));
    }
    return index;
}


int findAllPeaks(std::vector<float> pulse, std::vector<float> &xpeaks, std::vector<float> &ypeaks,
                 std::string polarity, float threshold) {

    // return error if there is no pulse or too short
    if(pulse.size() < 3) {
        return -1;
    }

    // loop over all samples looking for min value
    std::vector<float> peak;
    for(int i = 0; i < pulse.size(); i++) {
        if (abs(pulse.at(i)) < abs(threshold)) {
            if (peak.size() > 0) {
                auto j = findPeak(peak, polarity);
                xpeaks.push_back(j + i - peak.size());
                ypeaks.push_back(peak.at(j));
                peak.clear();
                // std::cout << peak.at(it) << std::endl;
            }
        }
        else {
            peak.push_back(pulse.at(i));
        }
    }

    // return error if there is no peak found
    if (xpeaks.size() == 0 || ypeaks.size() == 0){
        std::cout << "No valid peak found." << std::endl;
        return -1;
    }

    return xpeaks.size();
}


int findAbsolutePeak(int nsamples, short* pulse, std::string polarity) {
    int xpeak;
    float ypeak;

    // return error if there is no pulse
    if(nsamples <= 0 || !pulse) {
        return -1;
    }

    // loop over all samples looking for min value
    xpeak = 0;
    ypeak = pulse[20];

    for(int i = 21; i < nsamples-21; i++) {
        if (polarity == "neg"){
            if(pulse[i] < ypeak) {
                ypeak = pulse[i];
                xpeak = i;
            }
        }
        else if (polarity == "pos") {
            if(pulse[i] > ypeak) {
                ypeak = pulse[i];
                xpeak = i;
            }
        }
        else {
            if(abs(pulse[i]) > abs(ypeak)) {
                ypeak = pulse[i];
                xpeak = i;
            }
        }
    }

    return xpeak;
}


int findRealPeak(int nsamples, short* pulse, int rank, string option) {
    float xmin, ymin, tmp;

    // return error if there is no pulse
    if(nsamples <= 0 || !pulse) {
        return -1;
    }

    short pulse_flt[nsamples];
    for(int i = 0; i < nsamples; i++) {
        pulse_flt[i] = 0;
        tmp = 0;

        // filter with moving average
        if(option == "moving_average") {
            for(int j = -rank; j < rank + 1; j++) {
                tmp += pulse[i + j];
            }
            pulse_flt[i] = tmp / (rank + 1);
        }
        // filter with savitzky golay
        else if(option == "savitzky_golay") {
            pulse_flt[i] = 0;
        }
        // don't filter
        else {
            pulse_flt[i] = pulse[i];
        }
    }

    // loop over all samples looking for min value
    xmin = 0;
    ymin = pulse_flt[10];
    for(int i = 10; i < nsamples - 10; i++) {
        if(ymin > pulse_flt[i]) {
            ymin = pulse_flt[i];
            xmin = i;
        }
    }

    return xmin;
}


float gaussPeakTime(std::vector<float> times, std::vector<float> volts, float index_first, float index_last) {
    float mean, mean_err, chi2_red, tpeak;
    std::vector<float> errX, errY;

    auto fpeak = std::make_shared<TF1> ("fpeak", "gaus", index_first, index_last);

    // add errors for chi2
    for(int i = 0; i < times.size(); i++) {
        errX.push_back(0);
        errY.push_back(0);
    }
    
    auto pulse = std::make_shared<TGraphErrors> (times.size(), &times[0], &volts[0], &errX[0], &errY[0]);
    pulse->Fit("fpeak", "Q", "", index_first, index_last);
    mean = fpeak->GetParameter(1);
    mean_err = fpeak->GetParError(1);
    chi2_red = fpeak->GetChisquare() / fpeak->GetNDF();

    if(chi2_red > 10 || mean_err / mean > 0.1) {
        return -1;
    }
    tpeak = mean;

    return tpeak;
};



float risingEdgeTime(std::vector<float> times, std::vector<float> volts, float index_min, float fit_low, float fit_up, 
        float cfd, string option) {
    double x_low, x_up, xmin, ytmp, ymin;
    double slope, offset, chi2_red, ret;
    std::vector<float> errX, errY;
    
    // add errors for chi2
    for(int i = 0; i < times.size(); i++) {
        errX.push_back(0);
        errY.push_back(0);
    }
    auto pulse = std::make_shared<TGraphErrors> (times.size(), &times[0], &volts[0], &errX[0], &errY[0]);

    // get peak value
    pulse->GetPoint(index_min, xmin, ymin);

    // find first value below fit_low * ymin
    for(int i = 1; i < 100; i++) {
        pulse->GetPoint(index_min - i, x_low, ytmp);
        if(ytmp < fit_low * ymin) {
            break;
        }
    }

    // find first value below fit_up * ymin
    for(int i = 1; i < 100; i++) {
        pulse->GetPoint(index_min - i, x_up, ytmp);
        if(ytmp < fit_up * ymin) {
            break;
        }
    }

    // fit and extract timestamp
    auto flinear = std::make_shared<TF1> ("flinear", "[0]*x+[1]", x_low, x_up);

    pulse->Fit("flinear", "Q", "", x_low, x_up);
    slope = flinear->GetParameter(0);
    offset = flinear->GetParameter(1);
    chi2_red = flinear->GetChisquare() / flinear->GetNDF();

    // break if slope = 0 or chi2 too bad
    if(slope == 0 || chi2_red > 100) {
        return -1;
    }
    
    // return risetime or cfd timestamp
    if (option == "risetime") {
        ret = (0.90 * ymin - offset) / slope - (0.10 * ymin - offset) / slope;
    } 
    else if (option == "fixed") {
        ret = (cfd - offset) / slope;
    } 
    else {
        ret = (cfd * ymin - offset) / slope;
    }

    return ret;
};


float getBaseline(std::vector<float> pulse, int peak, int nbinsExcludedLeftOfPeak, int nbinsExcludedRightOfPeak) {
    float sum = 0;
    float cnt = 0;

    int nbins = pulse.size();

    // calculate mean of all samples outside of peak region
    // if(peak + nbinsExcludedRightOfPeak < nbins && nbinsExcludedRightOfPeak != -1) {
    //     for(int i = peak + nbinsExcludedRightOfPeak; i < nbins; i++) {
    //         sum += pulse.at(i);
    //         cnt += 1;
    //     }
    // }

    if(peak - nbinsExcludedLeftOfPeak > 0 && nbinsExcludedLeftOfPeak != -1) {
        for(int i = 0; i < peak - nbinsExcludedLeftOfPeak; i++) {
            sum += pulse.at(i);
            cnt += 1;
        }
    }

    // break if no cnts
    if(cnt == 0) {
        return -1;
    }

    return sum / cnt;
}


float getNoise(std::vector<float> pulse, int peak, int nbinsExcludedLeftOfPeak, int nbinsExcludedRightOfPeak) {
    float sum = 0;
    float cnt = 0;
    float var = 0;

    int nbins = pulse.size();

    // calculate mean and varriance of all samples outside of peak region
    // if(peak + nbinsExcludedRightOfPeak < nbins && nbinsExcludedRightOfPeak != -1) {
    //     for(int i = peak + nbinsExcludedRightOfPeak; i < nbins; i++) {
    //         sum += pulse.at(i);
    //         var += pow(pulse.at(i), 2);
    //         cnt += 1;
    //     }
    // }

    if(peak - nbinsExcludedLeftOfPeak > 0 && nbinsExcludedLeftOfPeak != -1) {
        for(int i = 0; i < peak - nbinsExcludedLeftOfPeak; i++) {
            sum += pulse.at(i);
            var += pow(pulse.at(i), 2);
            cnt += 1;
        }
    }

    // break if no cnts
    if(cnt == 0) {
        return -1;
    }

    // calculate variance
    var = (cnt * var - pow(sum, 2)) / pow(cnt, 2);

    return sqrt(var);
}


int substractBaseline(std::vector<float> pulse, int baseline) {
    for (auto& val : pulse) {
        val -= baseline;
    }
    return 0;
}


float getPulseIntegral(std::vector<float> pulse, int peak, int roi_left, int roi_right, std::string option) {
    float integral = 0;

    // calculate sum of all samples in full waveform
    if(option == "full") {
        for(int i = 0; i < pulse.size(); i++) {
            integral += pulse.at(i);
        }
    }

    // calculate sum of all samples within peak region
    else {
        if(peak - roi_left > 0 && peak + roi_right < pulse.size())
            for(int i = peak-roi_left; i < peak+roi_right; i++) {
                integral += pulse.at(i);
            }
    }

    return integral;
}



void plotWaveform(std::vector<float> pulse, int base, std::string out_file) {

    std::vector<float> csample, vsample;
    for(int i = 0; i < pulse.size(); i++) {
        csample.push_back(i);
        vsample.push_back(pulse.at(i) + base);
    }

    auto gr_wf = std::make_shared<TGraph> (csample.size(), &csample[0], &vsample[0]);
    gr_wf->SetMarkerSize(0.3);

    printGraph(gr_wf, out_file, "Sample Waveform", "# sample", "voltage (ADC)", -1.0, -1.0, 1.2);
}



std::shared_ptr<peakValues> analysePeak(std::vector<float> pulse, std::string polarity, int cut_left, int cut_right) {
    int id, peak;
    float amp, charge, base, base2, noise, trise, tpeak, t45;
    std::vector<float> times, volts;

    auto ret = std::make_shared<peakValues> ();

    int roi_left = 30;
    int roi_right = 100;

    peak = 0;
    peak = findPeak(std::vector<float>(pulse.begin()+cut_left, pulse.begin()+cut_right), polarity) + cut_left;

    base = getBaseline(pulse, peak, roi_left, roi_right);
    noise = getNoise(pulse, peak, roi_left, roi_right);
    substractBaseline(pulse, base);

    amp = pulse.at(peak);
    base2 = getBaseline(pulse, peak, roi_left, roi_right);
    charge = getPulseIntegral(pulse, peak, roi_left, roi_right, "");

    for(int i = 0; i < pulse.size(); i++) {
        times.push_back(i * 0.04);
        volts.push_back(pulse.at(i));
    }

    trise = risingEdgeTime(times, volts, peak, 0.2, 0.9, 0.45, "risetime");
    tpeak = gaussPeakTime(times, volts, -3, +3);
    t45 = risingEdgeTime(times, volts, peak, 0.2, 0.9, 0.45, "");

    ret->peak = peak;      // index of pulse sample holding the largest entry [-]
    ret->amp = amp;        // value of pulse sample holding the largest entry [adc]
    ret->charge = charge;  // integral of either full pulse or roi [adc]
    ret->base = base;      // mean of voltage samples outside of roi [adc]
    ret->base2 = base2;    // mean of voltage samples outside of roi [adc] after baseline substraction
    ret->noise = noise;    // std of voltage samples outside of roi [adc]
    ret->trise = trise;    // rise time of peak
    ret->tpeak = tpeak;    // mean of gauss fit around peak
    ret->t45 = t45;		   // cfd at 45% amplitude

    if (0) {
        std::cout << peak << " " << noise << " " << base << " " << base2
                  << " " << amp << " " << charge << " " << trise << " " << tpeak
                  << std::endl;
    }

    return ret;
}

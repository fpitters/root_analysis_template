/********************************************************************
 * File: analyse.cc
 * ------------------------
 *
 * Description:
 * Analyse something.
 *
 *
 * Version:
 * Author: Florian Pitters
 *
 *******************************************************************/

#include "analysis.h"


int analyse(){

    styleCommon();
    styleMap();
    styleGraph();

    Color_t kOneBlue = (new TColor(0/256., 43/256., 128/256.))->GetNumber();
    Color_t kOneMagenta = (new TColor(199/256., 68/256., 24/256.))->GetNumber();
    Color_t kOneOrange = (new TColor(221/256., 138/256., 13/256.))->GetNumber();
    Color_t kOneGreen = (new TColor(81/256., 153/256., 28/256.))->GetNumber();
    Color_t kOneCyan = (new TColor(24/256., 156/256., 199/256.))->GetNumber();
    Color_t kOneRed = (new TColor(109/256., 15/256., 14/256.))->GetNumber();

    Color_t kOneAzure = (new TColor(0/256., 153/256., 153/256.))->GetNumber();
    Color_t kOneOrange2 = (new TColor(221/256., 162/256., 13/256.))->GetNumber();
    Color_t kOneBlue2 = (new TColor(35/256., 75/256., 133/256.))->GetNumber();

    std::vector<Marker_t> markers = {kFullSquare, kFullCircle, kFullTriangleUp, kFullTriangleDown, kFullDiamond, kFullCrossX, kFullDoubleDiamond};
    std::vector<Style_t> styles = {1, 2, 4, 6, 7, 10};
    std::vector<Color_t> colours = {kOneBlue, kOneMagenta, kOneOrange, kOneGreen, kOneCyan, kOneOrange};


    // Preperations
    // ---------------------------------------

    int event;
    double val1, val2;
    std::string out_file;

    std::vector<double> x = {1, 2, 3, 4, 5, 6};
    std::vector<double> errx = {0, 0, 0, 0, 0, 0};
    std::vector<double> erry = {2, 1.8, 1.5, 1.3, 1.0, 1.0};
    std::vector<double> y1 = {18, 12, 8, 5, 4, 3};
    std::vector<double> y2 = {19, 13, 9, 6, 5, 4};
    std::vector<double> y3 = {20, 14, 10, 7, 6, 5};
    std::vector<double> y4 = {21, 15, 11, 8, 7, 6};
    std::vector<double> y5 = {22, 16, 12, 9, 8, 7};
    std::vector<double> y6 = {23, 17, 13, 10, 9, 8};
	
	

    // Analysis
    // --------------------------------------

	// some plot examples with random data
    auto hist = std::make_shared<TH1F> ("hist", "hist", 100, 0, 500);
    auto hist2 = std::make_shared<TH1F> ("hist2", "hist2", 100, 0, 500);
    auto map = std::make_shared<TH2F> ("map", "map", 100, 0, 500, 100, 0, 500);

    TRandom rnd;
    for (int i = 0; i < 10000; ++i) {
        val1 = rnd.Gaus(250, 20);
        hist->Fill(val1);
        val2 = rnd.Gaus(260, 40);
        hist2->Fill(val2);
        map->Fill(val1, val2);
    }
	
	// a waveform analysis example with real data
	// two lgad detectors with 10cm tof distance bombarded by a 194 MeV proton beam
    auto res = std::make_shared<runResults> ();
    res = analyse_parallel(dat_path, "", "test_data", "protons");
    


    // Print
    // ---------------------------------------

    styleGraph();
    out_file = res_path + "hist.pdf";
    printHist(hist, out_file, "Title", "x value (unit)", "y value (unit)",-1.0, -1.0, 1.5);

    styleMap();
    out_file = res_path + "map.pdf";
    printMap(map, out_file, "Title", "x value (unit)", "y value (unit)", "z value (unit)", -1.0, -1.0, 1.1, 1.1);

    // Multigraph
    styleGraph();
    out_file = res_path + "mg.pdf";
    auto canv_mg = std::make_shared<TCanvas> ("mg", "mg");
    auto mg_mg = std::make_shared<TMultiGraph> ();
    auto lg_mg = std::make_shared<TLegend> (0.68, 0.61, 0.88, 0.88); //
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y6[0], &errx[0], &erry[0], colours.at(5), markers.at(5), 1.2, 1, "lp", " label 6");
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y5[0], &errx[0], &erry[0], colours.at(4), markers.at(4), 1.2, 1, "lp", " label 5");
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y4[0], &errx[0], &erry[0], colours.at(3), markers.at(3), 1.2, 1, "lp", " label 4");
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y3[0], &errx[0], &erry[0], colours.at(2), markers.at(2), 1.2, 1, "lp", " label 1");
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y2[0], &errx[0], &erry[0], colours.at(1), markers.at(1), 1.2, 1, "lp", " label 2");
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y1[0], &errx[0], &erry[0], colours.at(0), markers.at(0), 1.2, 1, "lp", " label 3");
    mg_mg->Draw("A CP PLC PMC PFC");
    // mg_mg->Draw("A");
    mg_mg->SetTitle("Title");
    mg_mg->GetXaxis()->SetTitle("x label (unit)");
    mg_mg->GetYaxis()->SetTitle("y label (unit)");
    mg_mg->GetYaxis()->SetTitleOffset(1.1);
    mg_mg->GetXaxis()->SetLimits(0, 7);
    mg_mg->SetMinimum(0);
    mg_mg->SetMaximum(25);
    lg_mg->Draw();
    canv_mg->SaveAs(out_file.c_str());

    return 0;
}




std::shared_ptr<runResults> analyse_parallel(std::string path, std::string folder, std::string fn, std::string species) {

    styleCommon();
    styleMap();
    styleGraph();


    // Setup
    // ---------------------------------------

    float thr = 0.005;

    float nbins_time = 100;
    float vmin_time = -1;
    float vmax_time = +1;

    float nbins_amp = 100;
    float vmin_amp = 0;
    float vmax_amp = 0.1;

    float nbins_charge = 100;
    float vmin_charge = 0;
    float vmax_charge = 2.5;

    float nbins_noise = 100;
    float vmin_noise = 0;
    float vmax_noise = 0.001;

    if (species == "carbons"){
        vmax_charge = 15;
        vmax_amp = 0.4;
        thr = 0.01;
    }



    // Preperations
    // ---------------------------------------

    int event, ret, nevents;
    bool info = false;
    std::string out_file, dat_file_01, dat_file_02;
    std::vector<float> dat_test, xpeaks, ypeaks;
    std::vector<float> dat_01, peaks_01, dat_02, peaks_02;

    dat_file_01 = path + folder + "/" + fn + "_ch1.dat";
    dat_file_02 = path + folder + "/" + fn + "_ch2.dat";
    ret = read_array(dat_file_01, ' ', dat_01, false);
    std::cout << "-- Reading data file: " << dat_file_01 << ". " << dat_01.size() << " entries." << std::endl;
    ret = read_array(dat_file_02, ' ', dat_02, false);
    std::cout << "-- Reading data file: " << dat_file_02 << ". " << dat_02.size() << " entries." <<  std::endl;

	// waveforms are consecutive
	// 1000 samples per event
    nevents = static_cast<int> (dat_01.size()/1000.);
    
	if (dat_01.size() == 0 || dat_02.size() == 0) {
		std::cout << "-- Empty waveforms. Aborting. " << std::endl;
		return 0;
	}


    // Analysis
    // --------------------------------------
    
    auto vals_01 = std::make_shared<peakValues> ();
    auto vals_02 = std::make_shared<peakValues> ();

    std::cout << "-- Starting analysis. Found: " << nevents << " events." << std::endl;

	auto h1_noise = std::make_shared<TH1F>("", "", nbins_noise, vmin_noise, vmax_noise);
    auto h1_amp = std::make_shared<TH1F>("", "", nbins_amp, vmin_amp, vmax_amp);
    auto h1_charge = std::make_shared<TH1F>("", "", nbins_charge, vmin_charge, vmax_charge);
    auto h1_trise = std::make_shared<TH1F>("", "", 100, 0, 1);

    auto h2_noise = std::make_shared<TH1F>("", "", nbins_noise, vmin_noise, vmax_noise);
    auto h2_amp = std::make_shared<TH1F>("", "", nbins_amp, vmin_amp, vmax_amp);
    auto h2_charge = std::make_shared<TH1F>("", "", nbins_charge, vmin_charge, vmax_charge);
    auto h2_trise = std::make_shared<TH1F>("", "", 100, 0, 1);

    auto h_dt45 = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);

    // peak search
    int cnt_coinc = 0;
    int cnt_excpt = 0;
    for (int i = 0; i < nevents; i++){
        try {
            auto frame_01 = std::vector<float>(dat_01.begin() + i*1000, dat_01.begin() + i*1000 + 1000);
            auto frame_02 = std::vector<float>(dat_02.begin() + i*1000, dat_02.begin() + i*1000 + 1000);

            if (i%1000 == 0){
                out_file = res_path + folder + "/" + "wf_single_ch1_" + std::to_string(i) + "_" + fn + ".pdf";
                plotWaveform(frame_01, 0, out_file);
                out_file = res_path + folder + "/" + "wf_single_ch2_" + std::to_string(i) + "_" + fn + ".pdf";
                plotWaveform(frame_02, 0, out_file);
            }

            vals_01 = analysePeak(frame_01, "pos", 0, 700);
            vals_02 = analysePeak(frame_02, "pos", 0, 700);

            if ((vals_01->amp > thr) && (vals_02->amp > thr)) {

                // fill histogrammes
                h1_noise->Fill(vals_01->noise);
                h1_amp->Fill(vals_01->amp);
                h1_charge->Fill(vals_01->charge);
                h1_trise->Fill(vals_01->trise);
                h2_noise->Fill(vals_02->noise);
                h2_amp->Fill(vals_02->amp);
                h2_charge->Fill(vals_02->charge);
                h2_trise->Fill(vals_02->trise);
                h_dt45->Fill(vals_01->t45 - vals_02->t45);

                cnt_coinc++;
            }
        }
        catch (const std::out_of_range& e) {
            cnt_excpt++;
            cout << "An exception occurred." << '\n';
        }
    }

    // waveforms
    std::vector<float> x(5000);
    std::vector<float> errx(5000, 0);
    std::vector<float> erry(5000, 0);
    std::iota (std::begin(x), std::end(x), 0);
    for (int i = 0; i < 100; i++){
        auto peak_01 = std::vector<float>(dat_01.begin() + i*5000, dat_01.begin() + i*5000 + 5000);
        auto peak_02 = std::vector<float>(dat_02.begin() + i*5000, dat_02.begin() + i*5000 + 5000);
    }

    // fits
    auto res = std::shared_ptr<runResults> ();
    auto resf = std::shared_ptr<fitResult> ();
    resf = fitGauss(h_dt45, 0.1, -1, +1);
    

    // Print
    // ---------------------------------------

    styleGraph();
    out_file = res_path + folder + "/" + "h1_noise_" + fn + ".pdf";
    printHist(h1_noise, out_file, "", "noise (V)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h1_amp_" + fn + ".pdf";
    printHist(h1_amp, out_file, "", "waveform amplitude (V)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h1_charge_" + fn + ".pdf";
    printHist(h1_charge, out_file, "", "waveform integral (V*ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h1_trise_" + fn + ".pdf";
    printHist(h1_trise, out_file, "", "rise time (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h2_noise_" + fn + ".pdf";
    printHist(h2_noise, out_file, "", "noise (V)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h2_amp_" + fn + ".pdf";
    printHist(h2_amp, out_file, "", "waveform amplitude (V)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h2_charge_" + fn + ".pdf";
    printHist(h2_charge, out_file, "", "waveform integral (V*ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h2_trise_" + fn + ".pdf";
    printHist(h2_trise, out_file, "", "rise time (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt45_" + fn + ".pdf";
    printHist(h_dt45, out_file, "", "dt45 (ns)", "# events",-1.0, -1.0, 1.2);

    std::cout << "-- Analysed run " << folder << " Found: "
              << cnt_coinc << " coincidencent signals. "
              << cnt_excpt << " exceptions."
              << std::endl;
    std::cout << std::endl << std::endl << std::endl;

    return res;
}
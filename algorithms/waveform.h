/********************************************************************
* File: waveform.h
* ------------------------
*
* Description:
* Tools to analyse raw waveforms.
*
*
* Version:
* Author: Florian Pitters
*
*******************************************************************/


#ifndef __WAVEFORM_H_INCLUDED__
#define __WAVEFORM_H_INCLUDED__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "TTree.h"
#include "TBranch.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"

#include "common.h"




// --------------------------------------------------------
// Defintions
// --------------------------------------------------------


struct noiseValues{
	float alternate_sum;
	float direct_sum;
};


struct peakValues{
	int id;
	int peak;
	float amp;
	float charge;
	float base;
	float base2;
	float noise;
	float trise;
	float tpeak;
	float t45;
	unsigned int fQuality;
	unsigned int fSaturated;
};

int findPeak(std::vector<float> pulse, std::string polarity);
int findAllPeaks(std::vector<float> pulse, std::vector<float> &xpeaks,
				 std::vector<float> &ypeaks, std::string polarity, float threshold);

int findAbsolutePeak(int nsamples, short *pulse, std::string polarity);
int findRealPeak(int nsamples, short *pulse, int rank, std::string option);
float getPulseIntegral(std::vector<float> pulse, int peak, int roi_left, int roi_right, std::string option);
float getNoise(std::vector<float> pulse, int peak, int nbinsExcludedLeftOfPeak, int nbinsExcludedRightOfPeak);
float getBaseline(std::vector<float>, int peak, int nbinsExcludedLeftOfPeak, int nbinsExcludedRightOfPeak);
float risingEdgeTime(std::vector<float> times, std::vector<float> volts, float index_min, float fit_low, float fit_up, float cfd, std::string option);
float gaussPeakTime(std::vector<float> times, std::vector<float> volts, float index_first, float index_last);

int substractBaseline(std::vector<float> pulse, int baseline);

void plotWaveform(std::vector<float> pulse, int base, std::string out_file);
std::shared_ptr<peakValues> analysePeak(std::vector<float> pulse, std::string polarity, int cut_left, int cut_right);


#endif

/********************************************************************
 * File: analysis.h
 * ------------------------
 *
 * Description:
 *
 * Version:
 * Author: Florian Pitters
 *
 *******************************************************************/

#ifndef __ANALYSIS_H_INCLUDED__
#define __ANALYSIS_H_INCLUDED__

#include "common.h"
#include "waveform.h"


// --------------------------------------------------------
// Methods
// --------------------------------------------------------


struct runResults{
	float freq1_mean;
	float freq1_err;
	float freq1_rms;
	float amp1_mean;
	float amp1_err;
	float amp1_rms;
	float charge1_mean;
	float charge1_err;
	float charge1_rms;
	float noise1_mean;
	float noise1_err;
	float noise1_rms;
	float trise1_mean;
	float trise1_err;
	float trise1_rms;

	float freq2_mean;
	float freq2_err;
	float freq2_rms;
	float amp2_mean;
	float amp2_err;
	float amp2_rms;
	float charge2_mean;
	float charge2_err;
	float charge2_rms;
	float noise2_mean;
	float noise2_err;
	float noise2_rms;
	float trise2_mean;
	float trise2_err;
	float trise2_rms;

	float dt45_mean;
	float dt45_err;
	float dt45_rms;
	float dt45_std;
};


int analyse();
std::shared_ptr<runResults> analyse_parallel(std::string path, std::string folder, std::string fn, std::string species);

#endif

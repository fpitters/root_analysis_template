Project template for waveform analysis in C++. Projects using this template can be found at
- https://gitlab.cern.ch/fpipper/hgcSystemTiming
- https://gitlab.cern.ch/fpipper/hgcFastTiming
- https://gitlab.com/pCT-MA/lgad-analysis/

You will need to install the [ROOT library](https://root.cern) for it to work. 


## Install
At the folder's root type
```
make  
make test
```

## Requirements
[ROOT library](https://root.cern)

## Usage
Configuration is in `./algorithms/common.h`. Get started by looking at `./algorithms/analysis.cc` and use it as a template for further analysis. `./algorithms/common.cc` holds some useful helper functions. 

Execute with e.g.
`./bin/run -a analyse`
